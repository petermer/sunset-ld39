local class = require 'lib/middleclass'

local Queue = class('Queue')

function Queue:initialize()
    self.first = 0
    self.last = -1
    self.list = {}
end

function Queue:push(value)
    local last = self.last + 1
    self.last = last
    self.list[last] = value
end

function Queue:pop()
    local first = self.first
    if first > self.last then error("List is empty") end
    local value = self.list[first]
    self.list[first] = nil        -- to allow garbage collection
    self.first = first + 1
    return value
end

function Queue:peek()
    local first = self.first
    if first > self.last then error("List is empty") end
    local value = self.list[first]
    return value
end


function Queue:size()
    return self.last - self.first + 1
end

return Queue