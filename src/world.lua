local World = {
    size = nil,
    discoverables = {},
    zones = {},
    player = nil
}

function World.initialize(map_path, radiation_map_path, width, height)
    local map_image = Loader.load_image('map', map_path, true)
    local radiation_map_image = Loader.load_image('radiation_map', radiation_map_path, true)

    World.size = Vector(width, height)
    World.inv_size = Vector(1/width, 1/height)

    World._parse_map_details(map_image)
    World._parse_radiation_map_details(radiation_map_image)

    Director.add_timed_storyline(2, {'intro', 'intro', 'intro', 'intro'})
    Director.add_timed_storyline(8, {'hint'})
    Director.add_event_storyline('in_radiation', 'danger')
    Director.add_event_storyline('needed_powerups', 'explore_more')
    Director.add_event_storyline('saw_star', 'main_star')
    Director.add_event_storyline('saw_star1', 'main_star')
    Director.add_event_storyline('saw_star2', 'main_star')
end

function World.apply_radiation(ship)
    assert(ship.class == Ship)

    ship:set_danger(false)
    for i, zone in pairs(World.zones) do
        if zone:contains(ship.pos) then
            ship:set_danger(true)
        end
    end
end

function World.radiation_distance(ship)
    assert(ship.class == Ship)

    local min_dist = 1000
    for _, r in pairs(World.zones) do
        local this_dist = Util.point_to_rect(ship.pos, r.top_left, r.bottom_right)
        if this_dist < min_dist then
            min_dist = this_dist
        end
    end

    return min_dist
end

function World.get_closest_entity(from, only_not_visited)
    local min_dist = 9999
    local min_entity = nil

    for _, e in pairs(World.discoverables) do
        local this_dist = (from.pos - e.pos):len()

        if this_dist < min_dist then
            if only_not_visited then
                if not e.visited then
                    min_dist = this_dist
                    min_entity = e
                end
            else
                min_dist = this_dist
                min_entity = e
            end
        end
    end

    return min_entity, min_dist
end

function World.check_end()
    if World.player:is_dead() then
        local reason = World.player:get_death_reason()
        local msg

        if reason == 'radiation' then
            msg = 'You died of radiation, sadly'
        elseif reason == 'low_fuel' then
            msg = 'You ran out of fuel'
        else
            msg = 'unhandled reason'
        end

        DialogueOverlay.set_permanent_overlay(msg)
    end
end

function World.add_player(coords)
    if World.player then
        return
    end

    World.player = Ship:new(coords:unpack())
end

function World.add_mothership(coords)
    table.insert(World.discoverables, MotherShip:new(coords:unpack()))
end

function World.add_beacon(coords, type)
    type = type or 'fuel'

    local p
    if type == 'fuel' then
        p = FuelPowerup:new(love.math.random(12,20))
    elseif type == 'efficiency' then
        p = EfficiencyPowerup:new(0.7)
    elseif type == 'hardening' then
        p = HardeningPowerup:new()
    elseif type == 'minimap' then
        p = MinimapPowerup:new()
    else
        return
    end

    local b = Beacon:new(coords.x, coords.y, {p})
    table.insert(World.discoverables, b)
end

function World.add_main_star(coords)
    table.insert(World.discoverables, MainStar:new(coords:unpack()))
end

function World._color_to_type(r, g, b, a)
    if r == 0 and g == 255 and b == 0 and a > 0 then
        return 'player'
    elseif r == 255 and g == 0 and b == 255 and a >= 128 then
        return 'powerup'
    elseif r == 0 and g == 0 and b == 255 and a > 0 then
        return 'fuel'
    elseif r == 0 and g == 0 and b == 0 and a > 0 then
        return 'mainstar'
    elseif r == 255 and g == 0 and b == 0 and a > 0 then
        return 'mothership'
    else
        return 'invalid'
    end
end

World._powerup_types = {
    [128] = 'efficiency',
    [129] = 'hardening',
    [130] = 'minimap'
}
function World._get_powerup_type(a)
    if World._powerup_types[a] then
        return World._powerup_types[a]

    else
        return 'invalid'
    end
end

function World._parse_map_details(image)
    local size = Vector(image:getDimensions())
    local data = image:getData()

    for x=1,size.x do
        for y=1,size.y do
            r, g, b, a = data:getPixel(x - 1, y - 1)

            if a > 0 then
                local world_coord = Vector(x / size.x, y / size.y)
                local type = World._color_to_type(r, g, b, a)

                world_coord = world_coord:permul(World.size) - World.size / 2

                print(world_coord, r, g, b, a)
                print(type)

                if type == 'powerup' then
                    World.add_beacon(world_coord, World._get_powerup_type(a))
                elseif type == 'fuel' then
                    World.add_beacon(world_coord, 'fuel')
                elseif type == 'player' then
                    World.add_player(world_coord)
                elseif type == 'mainstar' then
                    World.add_main_star(world_coord)
                elseif type == 'mothership' then
                    World.add_mothership(world_coord)
                end
            end
        end
    end
end

function World._radiation_corner_type(r, g, b, a)
    if r ~= 255 then
        return nil
    end

    if a == 128 then
        -- top left
        return 'top_left'
    elseif a == 131 then
        -- bottom right
        return 'bottom_right'
    end

    return 'corner'
end

function World._parse_radiation_map_details(image)
    local size = Vector(image:getDimensions())
    local data = image:getData()
    local top_left, bottom_right

    for x=1,size.x do
        for y=1,size.y do
            r, g, b, a = data:getPixel(x - 1, y - 1)

            if a > 0 then
                local world_coord = Vector(x / size.x, y / size.y)
                local type = World._radiation_corner_type(r, g, b, a)

                world_coord = world_coord:permul(World.size) - World.size / 2                
                if type == 'top_left' then
                    top_left = world_coord:clone()
                elseif type == 'bottom_right' then
                    bottom_right = world_coord:clone()
                end

            end
        end
    end

    assert(top_left and bottom_right)
    table.insert(World.zones, RadiationZone:new(top_left, bottom_right))
end

function World.normalize_coords(world_coords)
    return (World.size / 2 + world_coords):permul(World.inv_size)
end

return World