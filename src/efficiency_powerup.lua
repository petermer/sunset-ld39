local EfficiencyPowerup = class('EfficiencyPowerup', Powerup)

function EfficiencyPowerup:initialize(factor)
    Powerup.initialize(self, false, 'fuel efficiency module')

    self.efficiency = factor

    self.story_tag = 'efficiency'
end

function EfficiencyPowerup:apply_effect(target)
    if Powerup.can_receive_effect(self, target) then
        Powerup.apply_effect(self, target)
        target:add_efficiency(self.efficiency)
    end
end

return EfficiencyPowerup