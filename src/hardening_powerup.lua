local HardeningPowerup = class('HardeningPowerup', Powerup)

function HardeningPowerup:initialize()
    Powerup.initialize(self, false, 'radiation hardening module')

    self.story_tag = 'radiation'
end

function HardeningPowerup:apply_effect(target)
    if Powerup.can_receive_effect(self, target) then
        Powerup.apply_effect(self, target)
        target:set_hardened(true)
    end
end

return HardeningPowerup