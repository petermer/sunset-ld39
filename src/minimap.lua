local Minimap = {
    offset = Vector(0.5, 0.075),
    size = Vector(1, 1),
    enabled = false
}

function Minimap.initialize()
    local world_aspect_ratio = World.size.y / World.size.x

    Minimap.size = Game.width / 8 * Vector(1, world_aspect_ratio)
end

function Minimap.get_minimap_coords(w_coords)
    return World.normalize_coords(w_coords):permul(Minimap.size)
end

function Minimap.draw()

    if not Minimap.enabled then
        return
    end

    local map_coord

    local map_scale_factor = Minimap.size.x / World.size.x
    local off = Vector(Game.width, Game.height):permul(Minimap.offset) - Minimap.size/2
    love.graphics.setColor(unpack({0,0,0,255}))
    love.graphics.rectangle('fill', off.x, off.y, Minimap.size:unpack())


    love.graphics.setColor(255, 255, 255)
    love.graphics.translate(off:unpack())

    -- radiation zone
    map_coord = Minimap.get_minimap_coords(Game.zones[1].top_left)
    love.graphics.rectangle('line', map_coord.x, map_coord.y, (map_scale_factor * Game.zones[1].size):unpack())

    map_coord = Minimap.get_minimap_coords(World.player.pos)
    love.graphics.rectangle('fill', map_coord.x - 2.5, map_coord.y - 2.5, 5, 5)

    -- draw player circle of view
    if Game.debug then
        love.graphics.circle('line', map_coord.x, map_coord.y, WorldRenderer.render_zbounds.far * (Minimap.size.x / World.size.x))
    end

    -- player line of sight
    local fov_left = Vector.fromPolar(World.player.rotation + WorldRenderer.render_fov / 2, 10) + map_coord
    local fov_right = Vector.fromPolar(World.player.rotation - WorldRenderer.render_fov / 2, 10) + map_coord
    love.graphics.setColor(0, 0, 255, 255)
    love.graphics.line(map_coord.x, map_coord.y, fov_left.x, fov_left.y)
    love.graphics.line(map_coord.x, map_coord.y, fov_right.x, fov_right.y)
    love.graphics.setColor(255, 255, 255)

    for i, v in pairs(World.discoverables) do
        map_coord = (v.pos + World.size / 2):permul(World.inv_size)
        map_coord = map_coord:permul(Minimap.size)
        if v.visible == true then
            love.graphics.setColor(255, 255, 255, 128)
        end

        love.graphics.setFont(Loader.fonts['pico8'])
        if v.type == 'mothership' then
            -- love.graphics.rectangle('line', map_coord.x, map_coord.y, 5, 5)
            love.graphics.print('m', map_coord.x, map_coord.y)
        elseif v.type == 'mainstar' then
            love.graphics.print('s', map_coord.x, map_coord.y)
        else
            love.graphics.circle('fill', map_coord.x, map_coord.y, 3, 5)
        end
        love.graphics.setColor(255, 255, 255, 255)
    end

    if Game.debug then
        -- closest
        local closest, _ = World.get_closest_entity(World.player, true)
        if closest then
            map_coord = Minimap.get_minimap_coords(World.player.pos)

            love.graphics.line(map_coord.x, map_coord.y, Minimap.get_minimap_coords(closest.pos):unpack())
        end
    end

    love.graphics.origin()
end

return Minimap