local StoryLine = class('StoryLine')

function StoryLine:initialize()
    self.current_id = 1

    self.story_parts = {
        intro = {
[[
DAY: 48929013923e+10

Condition: N/A

RETURN TO BASE
]],
[[
Something's wrong.
]],
[[
I need to get some more fuel first. 
Then, I should check out the mothership.
]],
[[
ARROWS TO MOVE. Z TO INTERACT
]]
        },
        hint = {
[[
Good thing those ABIs are working properly.

(ABI: Adjacemt Beacon Indicators)

The Colony installed them to every ship shortly after the
big 2056 wreck
]]
        },
        filler = {
[[
Those beacons... They are here for a reason.
Why would someone place them randomly in space?
I've never seen them before.
]]
        },
        minimap = {
[[
I've never seen one of those.
Seems to be mostly right.

...

That's a long way to the mothership.

I wonder what this enclosed area means.
]]
        },
        efficiency = {
[[
Cool! An engine... efficiency module?
I guess I'll need that, my old ship is
not really liking this trip.
]]
        },
        radiation = {
[[
... Wait, 'radiation hardening' module?
What is going on here? It's like someone is leading me somewhere.
I guess I'll take it.
]]
        },
        danger = {
[[
Fuck that hurts.
]]
        },
        explore_more = {
[[
Now that I'm (speculatively) protected from radiation
AND can coast for a while without my fuel giving up, I
should have a look in this so called 'Radiation Zone'
]]
        },
        main_star = {
[[
wow.
]],
[[
Is this... That can't be...
]],
[[
FUCK! I checked my Astronomy for Dummies
handbook, and that's definitely the
fucking sun!

WHAT IS GOING ON?
]]
        }
    }
    self.part_pointers = {}

    for tag, _ in pairs(self.story_parts) do
        self.part_pointers[tag] = 1
    end
end

function StoryLine:next(tag)
    tag = tag or 'filler'

    if not self.story_parts[tag] then
        return ''
    end

    if tag == 'filler' then
        local random_filler = love.math.random(#self.story_parts.filler + 1)

        if random_filler > #self.story_parts.filler then
            return ''
        else
            return self.story_parts.filler[random_filler]
        end
    end

    if self.part_pointers[tag] <= #self.story_parts[tag] then
        local index = self.part_pointers[tag]
        self.part_pointers[tag] = index + 1
        return self.story_parts[tag][index]
    end

    return '..'
end

return StoryLine