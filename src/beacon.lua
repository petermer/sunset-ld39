local Beacon = class('Beacon', Entity)

function Beacon:initialize(x, y, powerups)
    Entity.initialize(self, x, y, 'beacon')

    self.powerups = powerups or {}
    self.size = love.math.random() * 0.7 + 0.1

    local selection = math.random(3)
    self.image = Loader.load_image(string.format('beacon%d', selection), string.format('assets/beacon%d.png', selection))
end

function Beacon:update(dt)
end

function Beacon:draw()
    if self.visible == false then
        return
    end

    local zoom = self.size / self.dist
    local image_dims = Vector(self.image:getDimensions())
    local perturb = 3 * math.cos(love.timer.getTime())
    
    if self.pickable then
        love.graphics.setColor(0, 0, 255)
    else
        love.graphics.setColor(255, 255, 255, (1 - self.dist) * 255)
    end
    love.graphics.draw(self.image, self.projection.x - image_dims.x / 2 * zoom, perturb + self.projection.y - image_dims.y / 2 * zoom, 0, zoom, zoom)

    if Game.debug then
        love.graphics.setColor(0,0,0)
        love.graphics.print(string.format('%s_%02d', self.type, self.id), self.projection.x, self.projection.y + 10)
        love.graphics.print(string.format('dist: %.2f zoom: %.2f', self.dist, zoom), self.projection.x,  self.projection.y + 20)
    end
    love.graphics.setColor(255,255,255)
end

return Beacon