local HUD = {}

function HUD.initialize()
    Loader.load_image('hud_dark', 'assets/hud-dark.png')
    Loader.load_image('hud_light', 'assets/hud-light.png')
end

function HUD.draw()
    love.graphics.draw(Loader.images.hud_dark, 0, 0)