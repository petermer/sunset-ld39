local RadiationZone = class('RadiationZone')

function RadiationZone:initialize(top_left, bottom_right)
    self.top_left = top_left
    self.bottom_right = bottom_right
    self.size = bottom_right - top_left

    print(tostring(self.size))

    assert(bottom_right >= top_left)
    assert(Vector.isvector(top_left))
    assert(Vector.isvector(bottom_right))
end

function RadiationZone:contains(point)
    if point.x < self.top_left.x or point.x > self.bottom_right.x then
        return false
    elseif point.y < self.top_left.y or point.y > self.bottom_right.y then
        return false
    else
        return true
    end
end

function RadiationZone:get_size()
    return self.size
end

return RadiationZone