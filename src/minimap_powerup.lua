local MinimapPowerup = class('MinimapPowerup', Powerup)

function MinimapPowerup:initialize()
    Powerup.initialize(self, false, 'minimap')

    self.story_tag = 'minimap'
end

function MinimapPowerup:apply_effect(target)
    if Powerup.can_receive_effect(self, target) then
        Powerup.apply_effect(self, target)
        target:set_minimap(true)
    end
end

return MinimapPowerup