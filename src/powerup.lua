local Powerup = class('Powerup')

Powerup.static.id = 0

function Powerup:initialize(is_once, type)
    self.once = is_once or false
    self.id = Powerup.static.id
    self.type = type or 'powerup'
    self.story_tag = ''
    Powerup.static.id = Powerup.static.id + 1
end

function Powerup:can_receive_effect(target)
    return target.class == Ship
end

function Powerup:apply_effect(target)
    DialogueOverlay.queue_message(Game.storyline:next(self.story_tag))
end

return Powerup