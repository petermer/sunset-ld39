local director = {
    timer = 0,
    timed_q = nil,
    event_q = nil
}

function director.initialize()
    director.timed_q = {}
    director.event_q = {}
end

function director.add_timed_storyline(after, tags)
    for _, t in pairs(tags) do
        table.insert(director.timed_q, {
            timeout = after,
            tag = t
        })
    end
end

function director.add_event_storyline(after, tag)
    table.insert(director.event_q, {
        trigger = after,
        tag = tag
    })
end

function director.trigger(event)
    for i, s in pairs(director.event_q) do
        if s.trigger == event then
            table.insert(director.timed_q, {
                timeout = 0,
                tag = s.tag
            })

            director.event_q[i] = nil
        end
    end
end

function director.update(dt)
    for i, storyline in pairs(director.timed_q) do
        storyline.timeout = storyline.timeout - dt
        if storyline.timeout <= 0 then
            DialogueOverlay.queue_message(Game.storyline:next(storyline.tag))
            director.timed_q[i] = nil
        end
    end
end
            
return director