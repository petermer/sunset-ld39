local Entity = class('Entity')

Entity.static.id = 0

function Entity:initialize(x, y, type)
    self.pos = Vector(x or 0, y or 0)
    self.type = type or nil
    self.id = Entity.static.id
    self.powerups = {}
    self.story_tag = nil
    self.blueprints = {}

    self.pickable = false
    self.visible = true
    self.projection = Vector(0, 0)
    self.dist = 1000

    self.visited = false

    Entity.static.id = Entity.static.id + 1
end

function Entity:reset()
    self.pickable = false
    self.visible = false
end

function Entity:get_powerups()
    return self.powerups
end

function Entity:end_visit()
    for i, _ in pairs(self.powerups) do
        self.powerups[i] = nil
    end
    for i, _ in pairs(self.blueprints) do
        self.blueprints[i] = nil
    end

    self.visited = true
end

-- set projected stuff
function Entity:update_projection(x, y, dist)
    self.projection.x = x
    self.projection.y = y
    self.dist = dist
end

function Entity:set_pickable(is_pickable)
    self.pickable = is_pickable
end

function Entity:set_visible(is_visible)
    self.visible = is_visible
end

function Entity:update(dt)
end

function Entity:draw()
end

return Entity