local Background = {
    size_x = 1,
    size_y = 1,
    scale_x = 1,
    scale_y = 1,
    scroll = 0,
    scroll_speed = 0,
    scroll_accel = 260
}

function Background.initialize()
    Loader.load_image('background_sky', 'assets/stars.png')
    Background.size_x, Background.size_y = Loader.images.background_sky:getDimensions()

    Background.scale_x = Game.width / Background.size_x
    Background.scale_y = Game.height / Background.size_y
end

function Background.update(dt)
    local scroll = Background.scroll
    local speed = Background.scroll_speed
    local scaled_width =  Background.scale_y * Background.size_x
    local extra = math.max(0, scaled_width - Game.width)

    if scroll >= Game.width + extra then
        scroll = math.floor(scroll) % scaled_width
    elseif scroll <= -extra then
        scroll = scaled_width + scroll
    end

    Background.scroll = scroll
    Background.scroll_speed = speed
end

function Background.set_scroll(angle)
    local scroll = Background.scroll

    scroll = angle / (2 * math.pi) * Background.scale_y * Background.size_x

    Background.scroll = scroll
end

function Background.draw()
    -- Main background drawn on the right
    love.graphics.draw(Loader.images.background_sky, math.floor(Background.scroll), 0, 0, Background.scale_y, Background.scale_y)
    -- Filler background on the left side
    love.graphics.draw(Loader.images.background_sky, math.floor(Background.scroll) - Background.scale_y * Background.size_x, 0, 0, Background.scale_y, Background.scale_y)
end

return Background