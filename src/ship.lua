local Ship = class('Ship', Entity)

Ship.static.SPEED_LIMIT = Vector(-5, 7)

function Ship:initialize(x, y)
    Entity.initialize(self, x, y, 'ship')

    self.rotation = love.math.random() * math.pi - math.pi / 2
    self.rot_accel = 0
    self.rot_speed = 0

    self._ANG_ACCEL = 1.2
    self._LIN_ACCEL = 5
    if Game.debug then
        self._ANG_ACCEL = 1.2
        self._LIN_ACCEL = 7
        Ship.static.SPEED_LIMIT.y = 15
    end

    self.linear_accel = 0
    self.speed = 0

    self.fuel = 13
    self.fuel_usage = -0.4 -- fuel/sec
    self.efficiency = 1.0

    self.is_hardened = false -- radiation hardening, needed later
    self.in_radiation_zone = false
    self.radiation_timer = 5 -- 5 secs of radiation

    self.has_minimap = false

    self.crosshair = Loader.load_image('crosshair', 'assets/crosshair.png')

    self.indicator = Loader.load_image('indication-right', 'assets/indication-right.png')
    self.hull = Loader.load_image('hull', 'assets/hull.png')

    self.hint_text = love.graphics.newText(Loader.load_font('pico8', 'assets/mono.ttf'))
end

function Ship:collect_powerup(powerup)
    DialogueOverlay.set_timed_overlay(string.format('Picked up %s', powerup.type), 1)
    self.powerups[powerup.id] = powerup
    powerup:apply_effect(self)

    if self.is_hardened and self.efficiency < 1.0 then
        Director.trigger('needed_powerups')
    end

    Loader.load_sound('pickup', 'assets/sounds/powerup.wav'):play()
end

function Ship:add_fuel(fuel)
    self.fuel = self.fuel + fuel

    if self.fuel >= 100 then
        self.fuel = 0
    end
end

function Ship:add_efficiency(efficiency)
    self.efficiency = self.efficiency * efficiency
end

function Ship:set_hardened(is_hardened)
    self.is_hardened = is_hardened
end

function Ship:set_minimap(enable)
    self.has_minimap = enable

    if enable then
        Minimap.enabled = true
    else
        Minimap.enabled = false
    end
end

function Ship:set_danger(is_danger)
    self.in_radiation_zone = is_danger

    if is_danger and not self.is_hardened then
        Director.trigger('in_radiation')
    end
end

function Ship:is_dead()
    return self.radiation_timer < 0 or self.fuel <= 0
end

function Ship:get_death_reason()
    assert(self:is_dead())
    if self.radiation_timer < 0 then
        return 'radiation'
    elseif self.fuel <= 0 then
        return 'low_fuel'
    end
end

function Ship:update(dt)
    local is_rotation_damping = false
    local is_speed_damping = false
    local closest_entity_dist = WorldRenderer.closest_entity_dist(World.player)
    local pickables

    self.rot_accel = 0
    self.linear_accel = 0

    if Game.debug then
        if Input.was_pressed('1') then
            local p = FuelPowerup:new(15)
            self:collect_powerup(p)
        elseif Input.was_pressed('2') then
            local e = EfficiencyPowerup:new(0.83)
            self:collect_powerup(e)
        elseif Input.was_pressed('3') then
            local m = MinimapPowerup:new()
            self:collect_powerup(m)
        end
    end

    -- try to pick
    pickables = WorldRenderer.get_pickable()
    if Input.was_pressed('z') then
        -- hopefully just one
        for i, e in pairs(pickables) do
            local powerups = e:get_powerups()

            for j, p in pairs(powerups) do
                self:collect_powerup(p)
            end

            e:end_visit()

            break
        end
    end

    if self.fuel > 0 then
        if Input.pressed['left'] then
            self.rot_accel = -self._ANG_ACCEL
        elseif Input.pressed['right'] then
            self.rot_accel = self._ANG_ACCEL
        else
            is_rotation_damping = true
        end

        if Input.pressed['up'] then
            self.linear_accel = self._LIN_ACCEL
        elseif Input.pressed['down'] then
            -- slower reversing
            self.linear_accel = -self._LIN_ACCEL / 2
        else
            is_speed_damping = true
        end

        self.speed = self.speed + self.linear_accel * dt
        self.rot_speed = self.rot_speed + self.rot_accel * dt
    else
        is_rotation_damping, is_speed_damping = true, true
    end

    self.rot_speed = Util.clamp(self.rot_speed, -math.pi / 2, math.pi / 2)
    if closest_entity_dist > 10 then
        self.speed = Util.clamp(self.speed, Ship.static.SPEED_LIMIT:unpack())
    elseif closest_entity_dist <= 10 and closest_entity_dist > 1.5 then
        -- further clamp if entity close
        local slowdown = Util.rescale(closest_entity_dist, 1.5, 10, 0.01, 1)
        self.speed = Util.clamp(self.speed, (slowdown * Ship.static.SPEED_LIMIT):unpack())
    else
        self.speed = Util.clamp(self.speed, -1, 0)
    end

    self.pos = self.pos + self.speed * Vector(1, 0):rotated(self.rotation) * dt
    self.rotation = self.rotation + self.rot_speed * dt

    self.rotation = math.fmod(self.rotation, 2 * math.pi )

    if math.abs(self.linear_accel) > 0 or math.abs(self.rot_accel) > 0 then
        self.fuel = self.fuel + self.efficiency * self.fuel_usage * dt
    end

    if is_rotation_damping then
        self.rot_speed = 0.995 * self.rot_speed
        if math.abs(self.rot_speed) < 0.07 then
            self.rot_speed = 0
        end
    end

    if is_speed_damping then
        self.speed = 0.995 * self.speed
        if math.abs(self.speed) < 0.03 then
            self.speed = 0
        end
    end

    Background.set_scroll(2*math.pi - self.rotation)

    World.apply_radiation(self)
    if self.in_radiation_zone and not self.is_hardened then
        self.radiation_timer = self.radiation_timer - dt
    end
end

function Ship:draw()

    love.graphics.draw(self.hull, 0, 0)

    local cross_dim = Vector(self.crosshair:getDimensions())
    love.graphics.draw(self.crosshair, Game.width / 2 - cross_dim.x / 2, Game.height / 2 - cross_dim.y / 2)

    if WorldRenderer.get_pickable_count() > 0 then
        local text_width
        self.hint_text:set('scan (z)')
        text_width = self.hint_text:getWidth()
        
        love.graphics.draw(self.hint_text, Game.width / 2 - text_width / 2, Game.height / 2 + 20)
    end

    -- indicator
    local next_unvisited_entity, next_dist = World.get_closest_entity(self, true)

    if next_dist < WorldRenderer.render_zbounds.far and next_dist > 10 then
        local indicator_w, indicator_h = self.indicator:getDimensions()
        local angle_to_entity = (next_unvisited_entity.pos - self.pos):angleTo(Vector.fromPolar(self.rotation, 1))
        angle_to_entity = Util.unwrap(angle_to_entity)
        
        if angle_to_entity > WorldRenderer.render_fov / 2 then
            love.graphics.draw(self.indicator, Game.width - 64, Game.height / 2 - indicator_h / 2)
        elseif angle_to_entity < -WorldRenderer.render_fov / 2 then
            love.graphics.draw(self.indicator, indicator_w, Game.height / 2 + indicator_h / 2, math.pi)
        end
    end

    -- fuel
    love.graphics.rectangle('fill', 10, Game.height - 40, self.fuel / 100 * Game.width, 30)
    self.hint_text:set('FUEL')
    love.graphics.setColor(unpack({0,0,0,255}))
    love.graphics.draw(self.hint_text, 15, Game.height - 30)
    love.graphics.setColor(unpack({255,255,255,255}))

    if Game.debug then
        local start_draw_y = 50
        love.graphics.print(string.format('Radiation timer: %.2f', self.radiation_timer), 10, Game.height - start_draw_y)
        start_draw_y = start_draw_y + 15
        love.graphics.print(string.format('Fuel efficiency: %.1f', self.efficiency), 10, Game.height - start_draw_y)
        start_draw_y = start_draw_y + 15
        love.graphics.print(string.format('Fuel left: %.1f', self.fuel), 10, Game.height - start_draw_y)
        start_draw_y = start_draw_y + 15
        love.graphics.print(string.format('In radiation zone: %s', tostring(self.in_radiation_zone)), 10, Game.height - start_draw_y)
    end
end

return Ship