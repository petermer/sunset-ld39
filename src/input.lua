local Input = {
    pressed = {},
    pressed_once = {}
}

function Input.process(key, action)
    if action == 'down' then
        Input.pressed_once[key] = true
        Input.pressed[key] = true
    elseif action == 'up' then
        Input.pressed_once[key] = nil
        Input.pressed[key] = nil
    end
end

function Input.was_pressed(key)
    return Input.pressed_once[key] == true
end

function Input.update(dt)
    for i, v in pairs(Input.pressed_once) do
        Input.pressed_once[i] = nil
    end
end

return Input