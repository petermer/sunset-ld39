local MotherShip = class('MotherShip', Entity)

function MotherShip:initialize(x, y)
    Entity.initialize(self, x, y, 'mothership')
end

function MotherShip:draw()
    love.graphics.rectangle('line', self.projection.x, self.projection.y, 10, 10)
end

return MotherShip