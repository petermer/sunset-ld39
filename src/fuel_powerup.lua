local FuelPowerup = class('FuelPowerup', Powerup)

function FuelPowerup:initialize(amount)
    Powerup.initialize(self, true, 'fuel')

    self.fuel = amount
    self.story_tag = 'filler'
end

function FuelPowerup:apply_effect(target)
    if Powerup.can_receive_effect(self, target) then
        Powerup.apply_effect(self, target)

        target:add_fuel(self.fuel)
        self.fuel = 0 -- no naughties!
    end
end

return FuelPowerup