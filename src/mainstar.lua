local MainStar = class('MainStar', Entity)

function MainStar:initialize(x, y)
    Entity.initialize(self, x, y, 'mainstar')

    self.image = Loader.load_image('sun', 'assets/sun.png')
    self.size = Vector(self.image:getDimensions())
end

function MainStar:draw()
    if self.visible == false then
        return
    end

    if self.dist < 0.9 and self.dist >= 0.5 then
        Director.trigger('saw_star')
    elseif self.dist < 0.5 then
        Director.trigger('saw_star1')
        Director.trigger('saw_star2')
    end

    local zoom = 4 / self.dist
    
    if self.pickable then
        love.graphics.setColor(0, 0, 255)
    end
    love.graphics.setColor(255, 255, 255, Util.lerp(2, 255, (1-self.dist) ^ 2))
    love.graphics.draw(self.image, self.projection.x - self.size.x / 2 * zoom, self.projection.y - self.size.y / 2 * zoom, 0, zoom, zoom)

    if Game.debug then
        love.graphics.setColor(0,0,0)
        love.graphics.print(string.format('%s_%02d', self.type, self.id), self.projection.x, self.projection.y + 10)
        love.graphics.print(string.format('dist: %.2f zoom: %.2f', self.dist, zoom), self.projection.x,  self.projection.y + 20)
    end
    love.graphics.setColor(255,255,255)
end

return MainStar