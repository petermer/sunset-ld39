local WorldRenderer = {
    render_zbounds = {
        near = 0.01,
        far = 120
    },
    render_fov = math.rad(60),

    _cache = {
        pickable = {},
        visible = {},
        pickable_count = 0,
        visible_count = 0
    },

    canvas = nil,
    shaders = {},

    PICKABLE_DIST = 2.0
}

function WorldRenderer.initialize()
    WorldRenderer.canvas = love.graphics.newCanvas()
    WorldRenderer.shaders['damage'] = love.graphics.newShader([[
        extern number time;
        extern number dist;
        vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
        {
            vec4 cvColor = Texel(texture, texture_coords);
            number timeFactor = 0.5 * cos(time * 2) + 0.5;
            number distFactor = clamp(dist, 0, 50) / 50;
            number r = mix(cvColor.r, 1.0, pow(1 - distFactor, 3) * timeFactor);
            return vec4(r, cvColor.gba);
        }
    ]])
end

function WorldRenderer.get_pickable()
    return WorldRenderer._cache.pickable
end

function WorldRenderer.get_pickable_count()
    return WorldRenderer._cache.pickable_count
end

function WorldRenderer.closest_entity_dist(target)
    local min_dist = WorldRenderer.render_zbounds.far
    for _, p in pairs(WorldRenderer._cache.pickable) do
        local dist = (target.pos - p.pos):len()

        if dist < min_dist then
            min_dist = dist
        end
    end

    for _, v in pairs(WorldRenderer._cache.visible) do
        local dist = (target.pos - v.pos):len()
        
        if dist < min_dist then
            min_dist = dist
        end
    end

    return min_dist
end

function WorldRenderer.invalidate_cache()
    for i, _ in pairs(WorldRenderer._cache.pickable) do
        WorldRenderer._cache.pickable[i] = nil
    end
    WorldRenderer._cache.pickable_count = 0
    for i, _ in pairs(WorldRenderer._cache.visible) do
        WorldRenderer._cache.visible[i] = nil
    end
    WorldRenderer._cache.visible_count = 0
end

function WorldRenderer.cull(player, entities)
    local player_lookat = Vector.fromPolar(player.rotation, 1)
    local player_vec = player.pos

    WorldRenderer.invalidate_cache()

    for i, e in pairs(entities) do
        local dist = (player_vec - e.pos):len()

        e:reset()
        if dist <= WorldRenderer.render_zbounds.far and dist > WorldRenderer.render_zbounds.near then
            local angle_to_entity = (e.pos - player.pos):angleTo(player_lookat)

            angle_to_entity = Util.unwrap(angle_to_entity)
            
            if math.abs(angle_to_entity) <= WorldRenderer.render_fov / 2 then
                local entity_screen_x = (angle_to_entity / WorldRenderer.render_fov + 0.5) * Game.width

                if dist <= WorldRenderer.PICKABLE_DIST then
                    e:set_pickable(true)
                    WorldRenderer._cache.pickable[e.id] = e
                    WorldRenderer._cache.pickable_count = WorldRenderer._cache.pickable_count + 1
                end
                
                e:set_visible(true)
                WorldRenderer._cache.visible[e.id] = e
                WorldRenderer._cache.visible_count = WorldRenderer._cache.visible_count + 1

                -- watch for 0 z
                e:update_projection(entity_screen_x, Game.height / 2, (dist - WorldRenderer.render_zbounds.near) / (WorldRenderer.render_zbounds.far - WorldRenderer.render_zbounds.near))
            end
        end
    end
end

function WorldRenderer.render()
    love.graphics.setCanvas(WorldRenderer.canvas)
    love.graphics.clear()

    Background.draw()

    for i, e in pairs(WorldRenderer._cache.visible) do
        e:draw()
    end

    love.graphics.setCanvas()

    if not World.player.is_hardened then
        WorldRenderer.shaders['damage']:send('time', love.timer.getTime())
        WorldRenderer.shaders['damage']:send('dist', World.radiation_distance(World.player))
        love.graphics.setShader(WorldRenderer.shaders['damage'])
    end

    love.graphics.draw(WorldRenderer.canvas)
    love.graphics.setShader()
end

return WorldRenderer