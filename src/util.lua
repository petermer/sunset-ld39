local Util = {}

function Util.clamp(val, min, max)
    return math.min(max, math.max(min, val))
end

function Util.rescale(val, min, max, new_min, new_max)
    local rescaled_val = (((val - min) * (new_max - new_min)) / (max - min)) + new_min
    return rescaled_val
end

-- normalize angle
function Util.unwrap(angle)
    if angle > math.pi then
        angle = angle - 2 * math.pi
    elseif angle < -math.pi then
        angle = angle + 2 * math.pi
    end

    return angle
end

function Util.lerp(a, b, f)
    return a + f * (b - a)
end

function Util.point_to_rect(point, rect_a, rect_b)
    local width = rect_b.x - rect_a.x
    local height = rect_b.y - rect_a.y
    local x = rect_a.x + width / 2
    local y = rect_a.y + height / 2
    local dx = math.max(math.abs(point.x - x) - width / 2, 0);
    local dy = math.max(math.abs(point.y - y) - height / 2, 0);
    return math.sqrt(dx * dx + dy * dy);
end

return Util