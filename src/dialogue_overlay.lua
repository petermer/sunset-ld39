local queue = require 'lib/queue'

local DialogueOverlay = {
    font = nil,
    size = Vector(0, 0),
    text = nil,
    dismiss_text = nil,
    visible = false,
    visible_timer = 0,
    is_dismissable = false,
    is_danger = true,
    queue = {},
}

function DialogueOverlay.initialize()
    DialogueOverlay.font = Loader.load_font('pico8', 'assets/mono.ttf')
    DialogueOverlay.font:setLineHeight(1.2)

    DialogueOverlay.text = love.graphics.newText(DialogueOverlay.font, 'test')
    DialogueOverlay.dismiss_text = love.graphics.newText(DialogueOverlay.font, 'test')

    DialogueOverlay.queue = queue:new()

    Loader.load_sound('select', 'assets/sounds/blip.wav', true)
end

function DialogueOverlay.reset()
    DialogueOverlay.is_dismissable = false
    DialogueOverlay.is_danger = false
    DialogueOverlay.visible = false
    DialogueOverlay.visible_timer = 0
end

function DialogueOverlay.set_current_message(msg)
    DialogueOverlay.text:setf(msg, Game.width / 2, 'center')
    DialogueOverlay.size.x, DialogueOverlay.size.y = DialogueOverlay.text:getDimensions()
end

function DialogueOverlay.set_permanent_overlay(msg)
    if not msg then
        return
    end

    DialogueOverlay.visible = true
    DialogueOverlay.set_current_message(msg)
end

function DialogueOverlay.set_timed_overlay(msg, timeout)
    DialogueOverlay.visible = true
    DialogueOverlay.visible_timer = timeout
    DialogueOverlay.set_current_message(msg)
end

function DialogueOverlay.set_danger_overlay(msg, timeout)
    DialogueOverlay.set_timed_overlay(msg, timeout)
    DialogueOverlay.is_danger = true
end

function DialogueOverlay.queue_message(msg, timeout, dismissable)
    timeout = timeout or 5
    dismissable = dismissable or true

    if msg == '' then
        return
    end

    if dismissable then
        timeout = 999
    end

    DialogueOverlay.queue:push({
        msg = msg,
        timeout = timeout,
        dismissable = dismissable
    })
end

function DialogueOverlay.update(dt)
    DialogueOverlay.visible_timer = DialogueOverlay.visible_timer - dt

    if DialogueOverlay.visible_timer <= 0 then
        DialogueOverlay.reset()
        if DialogueOverlay.queue:size() > 0 then
            -- process next item
            local item = DialogueOverlay.queue:pop()
            DialogueOverlay.set_current_message(item.msg)
            DialogueOverlay.visible = true
            DialogueOverlay.visible_timer = item.timeout
            DialogueOverlay.is_dismissable = item.dismissable
        end
    else
        if Input.pressed_once['z'] and DialogueOverlay.is_dismissable then
            DialogueOverlay.visible_timer = 0
            Loader.sounds['select']:play()
        end
    end
end

function DialogueOverlay.draw()
    if DialogueOverlay.visible == false then
        return
    end

    local text = DialogueOverlay.text

    love.graphics.setColor(unpack({0, 0, 0, 255}))

    if DialogueOverlay.is_danger then
        love.graphics.clear(unpack({219, 30, 30, 255}))
    else
        love.graphics.clear(unpack({0, 0, 0, 255}))
    end
    love.graphics.setColor(unpack({255, 255, 255, 255}))

    love.graphics.draw(text, Game.width / 2 - Game.width / 4, Game.height / 2 - DialogueOverlay.size.y / 2)

    if DialogueOverlay.is_dismissable then
        local tip = DialogueOverlay.dismiss_text
        tip:set('z to dismiss')
        love.graphics.draw(tip, 10, Game.height - tip:getHeight() - 10)
    end

    DialogueOverlay.text = text
end

return DialogueOverlay