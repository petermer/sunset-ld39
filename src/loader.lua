local Loader = {
    images = {},
    fonts = {},
    sounds = {}
}

function Loader.load_image(asset_name, asset_path, skip_index)
    skip_index = skip_index or false

    if Loader.images[asset_name] then
        return Loader.images[asset_name]
    end

    local loaded_image = love.graphics.newImage(asset_path)
    if skip_index then
        return loaded_image
    else
        Loader.images[asset_name] = loaded_image
    end

    return loaded_image
end

function Loader.load_font(font_name, font_path)

    if Loader.fonts[font_name] then
        return Loader.fonts[font_name]
    end

    Loader.fonts[font_name] = love.graphics.newFont(font_path)

    return Loader.fonts[font_name]
end

function Loader.load_sound(sound_name, sound_path, static)

    if Loader.sounds[sound_name] then
        return Loader.sounds[sound_name]
    end

    if static then
        Loader.sounds[sound_name] = love.audio.newSource(sound_path, 'static')
    else
        Loader.sounds[sound_name] = love.audio.newSource(sound_path)
    end

    return Loader.sounds[sound_name]
end

return Loader