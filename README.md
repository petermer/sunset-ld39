# sunset #

*Made for the 39th Ludum Dare game competition*

Having been in the Colony’s fleet for many years, you usually mind your own business scavenging. Today, you get an emergency broadcast.

Welcome to sunset.

## Screenshots

![scr1](https://static.jam.vg/raw/c25/1/z/66be.png)
![src2](https://i.imgur.com/VhRn0r8.png)

## Instructions
If not using Windows, download the love2d executable for your platform from [here](https://love2d.org/) (works with v0.10.2)

Arrows to move, Z to interact, Q to quit.

Follow the arrows at the left/right side of the screen if you are lost. Beacons are hard to find but trust the indicators! You will find them by going forward eventually.

Collect enough fuel at first, so you can venture deeper into the unknown.

## Disclaimer
Consider this unfinished, as parts of the story are missing. The game is quite unbalanced in terms of fuel given / ship speed because I ran out of time.

Enjoy!