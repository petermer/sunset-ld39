class = require 'lib/middleclass'
Vector = require 'lib/vector'

-- game stuff
Background = require 'src/background'
Entity = require 'src/entity'

Beacon = require 'src/beacon'
Ship = require 'src/ship'
MainStar = require 'src/mainstar'
MotherShip = require 'src/mothership'

Powerup = require 'src/powerup'
EfficiencyPowerup = require 'src/efficiency_powerup'
FuelPowerup = require 'src/fuel_powerup'
HardeningPowerup = require 'src/hardening_powerup'
MinimapPowerup = require 'src/minimap_powerup'

RadiationZone = require 'src/radiationzone'

StoryLine = require 'src/storyline'
Director = require 'src/director'

DialogueOverlay = require 'src/dialogue_overlay'
Input = require 'src/input'
Loader = require 'src/loader'
Minimap = require 'src/minimap'
Util = require 'src/util'
World = require 'src/world'
WorldRenderer = require 'src/worldrenderer'

Game = {
    width = 0,
    height = 0,
    debug = true,
    storyline = nil
}

function love.load()
    love.graphics.setDefaultFilter('nearest', 'nearest', 1)
    Game.width, Game.height = love.window.getMode()

    Background.initialize()
    DialogueOverlay.initialize()
    Director.initialize()

    Loader.load_sound('ambient', 'assets/sounds/ambient.wav'):setLooping(true)
    Loader.sounds['ambient']:setVolume(0.5)
    Loader.sounds['ambient']:play()

    Game.storyline = StoryLine:new()

    World.initialize('assets/map.png', 'assets/radiation.png', 512, 256)
    WorldRenderer.initialize()
    Game.zones = World.zones

    Minimap.initialize()

    if Game.debug then
        io.stdout:setvbuf 'no'
        Minimap.enabled = true
    end
end

function love.update(dt)
    Director.update(dt)
    DialogueOverlay.update(dt)

    if not DialogueOverlay.visible then
        WorldRenderer.cull(World.player, World.discoverables)

        World.player:update(dt)
        Background.update(dt)


        World.check_end()
    end

    if Input.pressed['q'] then
        love.event.quit()
    end

    if Game.debug and Input.pressed_once['m'] then
        DialogueOverlay.queue_message(Game.storyline:next())
    end

    Input.update(dt)
end

function love.draw()
    WorldRenderer.render()
    World.player:draw()

    -- HUD
    Minimap.draw()

    DialogueOverlay.draw()

    if Game.debug then
        love.graphics.print(string.format('Ship pos: %.2f %.2f', World.player.pos.x, World.player.pos.y), 10, 20)
        love.graphics.print('Ship rotation: ' .. math.deg(World.player.rotation), 10, 40)
    end
end

function love.keypressed(key, scancode, isrepeat)
    Input.process(key, 'down')
end

function love.keyreleased(key, scancode, isrepeat)
    Input.process(key, 'up')
end