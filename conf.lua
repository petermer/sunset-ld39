function love.conf(t)
    t.console = false
    t.window.width = 1440
    t.window.height = 600
    t.window.vsync = false
end